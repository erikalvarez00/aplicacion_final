class CreateMemberships < ActiveRecord::Migration[5.0]
  def change
    create_table :memberships do |t|
      t.integer :id_cliente
      t.integer :id_servicio
      t.date :fecha_inicio
      t.date :fecha_fin

      t.timestamps
    end
  end
end
