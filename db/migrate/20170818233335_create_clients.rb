class CreateClients < ActiveRecord::Migration[5.0]
  def change
    create_table :clients do |t|
      t.integer :id_usuario
      t.string :foto
      t.string :nombre
      t.string :apellidos

      t.timestamps
    end
  end
end
