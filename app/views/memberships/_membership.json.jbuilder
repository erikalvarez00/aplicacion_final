json.extract! membership, :id, :id_cliente, :id_servicio, :fecha_inicio, :fecha_fin, :created_at, :updated_at
json.url membership_url(membership, format: :json)
