json.extract! client, :id, :id_usuario, :foto, :nombre, :apellidos, :created_at, :updated_at
json.url client_url(client, format: :json)
